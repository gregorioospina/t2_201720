package model.data_structures;
import java.util.Iterator;

public class RingList <T> implements IList<T>{

	private Node<T> primero;
	private int tamanho;

	public RingList()
	{
		primero = null;
		tamanho = 0;
	}

	public int Size()
	{
		return tamanho;
	}

	public void agregar(T data)
	{
		Node<T> nuevo = new Node<T>(data, null, null);
		tamanho++;
		if(primero == null)
		{
			primero = nuevo;
		}
		else
		{
			primero.cambiarPrev(nuevo);
			nuevo.cambiarNext(primero);
			primero = nuevo;
		}

	}

	public Node<T> get(int index)
	{
		int contador1 = 0;
		int busq = index%Size();
		Node<T> buscador = primero;
		while(contador1<busq)
		{
			buscador = buscador.darNext();
			contador1 ++;
		}
		return buscador;
	}

	public void eliminarNodo(int index)
	{
		int indexX = index%Size();
		Node<T> aEliminar = get(index);
		
		if(indexX == 0)
		{
			primero = aEliminar.darNext();
			tamanho--;
		}

		else
		{
			aEliminar.darNext().cambiarPrev(aEliminar.darPrev());
			aEliminar.darPrev().cambiarNext(aEliminar.darNext());
			tamanho--;
		}
	}
	
	public boolean isEmpty()
	{
		if (primero == null)
		{
			return true;
		}
		return false;
	}
	
	public Node<T> getLast()
	{
		int last = this.tamanho-1;
		return this.get(last);
		
	}


	@Override
	public Iterator iterator() 
	{
		// TODO Auto-generated method stub

		Iterator<T> it = new Iterator<T>()
				{

			int contador = 0;
			@Override
			public boolean hasNext() 
			{
				boolean respuesta = false;
				try
				{
					// TODO Auto-generated method stub
					if(get(contador) == null)
						respuesta = true;
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
				return respuesta;
			}

			@Override
			public T next() 
			{
				T rta = null;
				// TODO Auto-generated method stub
				try 
				{
					rta = get(contador).giveInfo();
				}
				catch (Exception e) 
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return rta;
			}

				};
				return it;
	}

	@Override
	public void add(Node n) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addAtEnd(Node n) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addAtK(Node n, int k) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public Node getElement(int k) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Node getCurrentElement() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getSize() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void delete() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAtK(int k) {
		// TODO Auto-generated method stub
		
	}

}
