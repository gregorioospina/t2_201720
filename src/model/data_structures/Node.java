package model.data_structures;

public class Node<T> {

	
	private Node<T> next;
	private Node<T> previous;
	private T data;
	
	
	public Node(T datax)
	{
		this.data = datax;
		next = null;
		previous = null;
	}
	
	public Node(T initialData, Node<T> newNext, Node<T> newPrev)
	{
		this.data = initialData;
		this.next = newNext;
		this.previous = newPrev;
	}
	
	public String toString(){
		return data + "";
	}
	
	public void nuevoNodoDespues(T aAgregar)
	{
		this.next = new Node<T> (aAgregar, next, this);
		
	}
	
	public void nuevoNodoAntes(T aAgregar)
	{
		this.previous = new Node<T> (aAgregar, this, previous);
	}
	
	public Node<T> darNext()
	{
		return next;
	}
	
	public Node<T> darPrev()
	{
		return previous;
	}
	
	public void cambiarNext(Node<T> nuevoNext)
	{
		this.next = nuevoNext;
	}
	
	public void cambiarPrev(Node<T> nuevoPrev)
	{
		this.previous = nuevoPrev;
	}
	
	public T giveInfo()
	{
		return data;
	}
	
	
	
	
			
}