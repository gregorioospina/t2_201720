package model.data_structures;

import java.util.Iterator;

public class DoubleLinkedList <T> implements IList {
	
	private Node<T> primero;
	private Node<T> ultimo;
	private int tamanho;
	
	public DoubleLinkedList()
	{
		primero = null;
		ultimo = null;
		tamanho = 0;
	}
	
	public void agregarComienzo(T data)
	{
		Node<T> nuevo = new Node<T>(data, null, null);
		tamanho++;
		if(primero == null)
		{
			primero = nuevo;
			ultimo = primero;
		}
		else
		{
			primero.cambiarPrev(nuevo);
			nuevo.cambiarNext(primero);
			primero = nuevo;
		}
		
	}
	
	public void agregarFinal(T data)
	{
		Node<T> nuevo = new Node<T>(data, null, null);
		tamanho++;
		if(ultimo == null)
		{
			ultimo = nuevo;
			primero = ultimo;
		}
		else
		{
			ultimo.cambiarNext(nuevo);
			nuevo.cambiarPrev(ultimo);
			ultimo = nuevo;
			
		}
		
	}
	
	public Node<T> get(int index) throws Exception
	{
		if(index>getSize() || index < 0)
		{
			throw new Exception("Not a valid index");
		}
		else
		{
			Node<T> buscador = primero;
			int contador = 0;
			while(contador<index)
			{
				buscador = buscador.darNext();
				contador ++;
			}
			return buscador;
		}
		
		
	}
	
	public boolean isEmpty()
	{
		if (primero == null || ultimo == null)
		{
			return true;
		}
		return false;
	}
	
	public boolean contieneA(Node<T> nodo) throws Exception
	{
		
		for(int i =0; i<this.getSize(); i++)
		{
			if(this.get(i).equals(nodo))
			{
				return true;
			}
		}
		return false;
	}
	
	public Node<T> darPrimero()
	{
		return primero;
	}
	
	public Node<T> darUltimo()
	{
		return ultimo;
	}

	@Override
	public Iterator iterator() 
	{
		// TODO Auto-generated method stub

				Iterator<T> it = new Iterator<T>()
						{
					
							int contador = 0;
							@Override
							public boolean hasNext() 
							{
								boolean respuesta = false;
								try
								{
								// TODO Auto-generated method stub
								if(get(contador) == null)
								respuesta = true;
								}
								catch(Exception e)
								{
									e.printStackTrace();
								}
								return respuesta;
							}

							@Override
							public T next() 
							{
								T rta = null;
								// TODO Auto-generated method stub
								try 
								{
									rta = get(contador).giveInfo();
								}
								catch (Exception e) 
								{
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								return rta;
							}

						};
						return it;
	}

	@Override
	public void add(Node n) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addAtEnd(Node n) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addAtK(Node n, int k) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Node getElement(int k) {
		// TODO Auto-generated method stub
		{
			Node<T> buscador = primero;
			int contador = 0;
			while(contador<k)
			{
				buscador = buscador.darNext();
				contador ++;
			}
			return buscador;
		}
		
		
	}

	@Override
	public Node getCurrentElement() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getSize() {
		// TODO Auto-generated method stub
		return tamanho;
	}

	@Override
	public void delete() 
	{
		// TODO Auto-generated method stub
		if(tamanho == 1)
		{
			primero = null;
			ultimo = null;
		}
		else if(tamanho == 0)
		{
			System.out.println("El tama�o es 0");
		}
		else
		{
			primero = primero.darNext();
		}
	}

	@Override
	public void deleteAtK(int k) 
	{
		// TODO Auto-generated method stub
		try
		{
			Node<T> aEliminar = get(k);
			if(k == 0)
			{
				primero = aEliminar.darNext();
				System.out.println("Entra");
				tamanho --;
			}
			else if(k == Size()-1)
			{
				ultimo = aEliminar.darPrev();
				aEliminar.darPrev().cambiarNext(null);
				tamanho --;
			}
			else
			{
				aEliminar.darNext().cambiarPrev(aEliminar.darPrev());
				aEliminar.darPrev().cambiarNext(aEliminar.darNext());
				tamanho --;
			}
		}
		catch(Exception e)
		{
			System.out.println("Numero de indice invalido");
		}
		
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
