package model.logic;

import java.util.ArrayList;

import model.data_structures.DoubleLinkedList;
import model.data_structures.IList;
import model.data_structures.Node;
import model.data_structures.RingList;
import model.vo.VOStop;

public class QuickSort<T> {

	public void quickSort(ArrayList<VOStop> arr, int bajo, int alto)
	{
		if(bajo >= alto)
		{
			return;
		}
		
		int middle = bajo +(alto - bajo)/2;
		VOStop pivot = arr.get(middle);
		
		int i = bajo;
		int j = alto;
		
		while(i<=j)
		{
			while(arr.get(i).id() < pivot.id())
			{
				i++;
			}
			while(arr.get(j).id() < pivot.id())
			{
				j--;
			}
			if(i<=j)
			{
				VOStop temp = arr.get(i);
				arr.set(i, arr.get(j));
				arr.set(j, temp);
			}
			System.out.println(arr.get(i).id());
		}
		if(bajo<j)
			quickSort(arr, bajo, j);
		if(alto>i)
			quickSort(arr, i, alto);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

//	private RingList<VOStop> array;
//	private int tamanho;
//
//	public void sort(RingList<VOStop> arregloInicial)
//	{
//		this.array = arregloInicial;
//		tamanho = arregloInicial.Size();
//		preSort(0, tamanho -1);
//	}
//
//	private void preSort(int bajo, int alto)
//	{
//		int x = bajo;
//		int y = alto;
//
//		Node<VOStop> pivot = array.get(bajo +(alto - bajo)/2);
//
//		while(x <= y)
//		{
//			while(array.get(x).giveInfo().zoneID().compareTo(pivot.giveInfo().zoneID()) > 0)
//			{
//				x++;
//			}
//			while(array.get(y).giveInfo().zoneID().compareTo(pivot.giveInfo().zoneID()) > 0)
//			{
//				y++;
//			}
//			if(x<=y)
//			{
//				intercambio(x, y);
//				
//				x++;
//				y--;
//			}
//		}
//		if(bajo<y)
//		{
//			preSort(bajo, y);
//		}
//		if(x<alto)
//		{
//			preSort(x, alto);
//		}
//	}
//	
//	private void intercambio(int x, int y)
//	{
//		Node<VOStop> xx = array.get(x);
//		Node<VOStop> yy = array.get(y);
//		
//		if(xx.darPrev() != null && yy.darPrev() != null)
//		{
//			Node<VOStop> temp = array.get(x);
//			xx.darNext().cambiarPrev(yy);
//			xx.darPrev().cambiarNext(yy);
//			
//			xx.cambiarNext(yy.darNext());
//			xx.cambiarPrev(yy.darPrev());
//			
//			yy.darNext().cambiarPrev(xx);
//			yy.darPrev().cambiarNext(xx);
//			
//			yy.cambiarPrev(temp.darPrev());
//			yy.cambiarNext(temp.darNext());
//			
//		}
//		else if(xx.darPrev() == null && yy.darNext() != null)
//		{
//			Node<VOStop> temp = array.get(x);
//			xx.darNext().cambiarPrev(yy);
//			
//			xx.cambiarNext(yy.darNext());
//			xx.cambiarPrev(yy.darPrev());
//			
//			yy.darNext().cambiarPrev(xx);
//			yy.darPrev().cambiarNext(xx);
//			
//			yy.cambiarPrev(null);
//			yy.cambiarNext(temp.darNext());
//		}
//		else if(xx.darPrev() != null && yy.darNext() == null)
//		{
//			Node<VOStop> temp = array.get(x);
//			xx.darNext().cambiarPrev(yy);
//			xx.darPrev().cambiarNext(yy);
//			
//			xx.cambiarNext(null);
//			xx.cambiarPrev(yy.darPrev());
//			
//			yy.darPrev().cambiarNext(xx);
//			
//			yy.cambiarPrev(temp.darPrev());
//			yy.cambiarNext(temp.darNext());
//		}
//	}
//	
//	

}


