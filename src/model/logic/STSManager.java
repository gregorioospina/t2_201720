package model.logic;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import com.sun.xml.internal.bind.v2.schemagen.xmlschema.List;

import api.ISTSManager;
import model.vo.VORoute;
import model.vo.VOStop;
import model.vo.VOStoptimes;
import model.vo.VOTrips;
import model.data_structures.DoubleLinkedList;
import model.data_structures.IList;
import model.data_structures.Node;
import model.data_structures.RingList;

public class STSManager <T> implements ISTSManager {

	private DoubleLinkedList<VORoute> Routes;
	private RingList<VOTrips> Trips;
	private DoubleLinkedList<VOStoptimes> StopTimes;
	private RingList<VOStop> Stops;
	private RingList<RingList<VOTrips>> ListaTripsOrganizada;
	private RingList<RingList<VOStop>> ListaStopsOrganizada;
	private RingList<VOStop> StopsAlfanumericamente;



	@Override
	public void loadRoutes(String routesFile) 
	{
		String linea;
		BufferedReader br = null;
		String splitBy = ",";

		// TODO Auto-generated method stub
		try 
		{

			//			FileReader fr = new FileReader(routesFile);
			//			 new BufferedReader(fr);
			br = new BufferedReader(new FileReader(routesFile));
			Routes = new DoubleLinkedList<VORoute>();
			br.readLine();
			while( (linea = br.readLine()) != null)
			{
				String[] lineas = linea.split(splitBy);
				int routId = Integer.parseInt((lineas[0]));
				String agencyId = lineas[1];
				String routeShortName = lineas[2];
				String routeLongName = lineas[3];
				String routeDescription = lineas[4];
				String routeType = lineas[5];
				String routeUrl = lineas[6];
				String routeColor = lineas[7];
				String routeTextColor = lineas[8];


				VORoute vor = new VORoute(routId, agencyId, routeShortName, routeLongName, routeDescription, routeType, routeUrl, routeColor, routeTextColor);
				Routes.agregarFinal(vor);
			}

			br.close();


		} 
		catch (IOException e1) 
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}		
	}


	//	public void listaRecursivaTrips()
	//	{
	//		RingList<VOTrips> listaRecursiva = new RingList<VOTrips>();
	//		Node<VOTrips> x = Trips.get(0);
	//		listaRecursiva.add(x);
	//		while(x.darNext().giveInfo().routeId() == x.giveInfo().routeId())
	//		{
	//			listaRecursiva.add(x);
	//			x = x.darNext();
	//			Trips.eliminarNodo(0);
	//		}
	//		ListaTripsOrganizada.agregar(listaRecursiva);
	//		listaRecursivaTrips();
	//
	//	 }

	public void BubbleSortStops(RingList<VOStop> lista)
	{
		int contador = 0;
		Node<VOStop> temp;

		System.out.println(contador +"");
		for(int i = 0; i<lista.Size(); i++)
		{
			System.out.println(contador +"");
			contador++;
			for(int j = 1; j<lista.Size();j++)
			{
				if(lista.get(j-1).giveInfo().zoneID().compareTo(lista.get(j).giveInfo().zoneID()) > 0)
				{
					contador++;
					temp = lista.get(j-1);
					temp.cambiarNext(lista.get(j).darNext());
					temp.cambiarPrev(lista.get(j));
					lista.get(j).cambiarNext(temp);
					lista.get(j).cambiarPrev(temp.darPrev());
				}
			}
		}
	}

		public void listaRecursivaStops()
		{
			RingList<VOStop> listaRecursiva = new RingList<VOStop>();
			Node<VOStop> x = StopsAlfanumericamente.get(0);
			listaRecursiva.add(x);
			while(x.darNext().giveInfo().zoneID().equals(x.giveInfo().zoneID()))
			{
				listaRecursiva.add(x);
				x = x.darNext();
				StopsAlfanumericamente.eliminarNodo(0);
			}
			ListaStopsOrganizada.agregar(listaRecursiva);
			System.out.println("recur bien");
			listaRecursivaStops();
	
	
		}

	public void listaRecursivaStops(RingList<RingList<VOStop>> lista, Node<VOStop> elemento)
	{
		if(elemento.darNext() != null){	
		}
		else
		{
			if((lista.getLast().giveInfo().get(0).giveInfo().zoneID().compareTo(elemento.giveInfo().zoneID()))==0)
			{
				lista.getLast().giveInfo().add(elemento);
			}
			else
			{
				RingList<VOStop> n = new RingList<VOStop>();
				lista.agregar(n);
				n.agregar(elemento.giveInfo());
			}
			listaRecursivaStops(lista, elemento.darNext());
		}
	}


	public void listaRecursivaTrips(RingList<RingList<VOTrips>> lista, Node<VOTrips> elemento)
	{
		if(elemento.darNext() != null){	
		}
		else
		{
			if((lista.getLast().giveInfo().get(0).giveInfo().tripID().compareTo(elemento.giveInfo().tripID()))==0)
			{
				lista.getLast().giveInfo().add(elemento);
			}
			else
			{
				RingList<VOTrips> n = new RingList<VOTrips>();
				lista.agregar(n);
				n.agregar(elemento.giveInfo());
			}
			listaRecursivaTrips(lista, elemento.darNext());
		}
	}



	@Override
	public void loadTrips(String tripsFile) throws IOException
	{
		String linea;
		BufferedReader br = null;
		String splitBy = ",";

		// TODO Auto-generated method stub
		try 
		{
			br = new BufferedReader(new FileReader(tripsFile));
			Trips = new RingList<VOTrips>();
			br.readLine();
			while( (linea = br.readLine()) != null)
			{
				String[] lineas = linea.split(splitBy);
				int routId = Integer.parseInt((lineas[0]));
				String serviceID = lineas[1];
				String tripID = lineas[2];
				String headsign = lineas[3];
				String sName = lineas[4];
				int direccionID = Integer.parseInt(lineas[5]);
				String blockID = lineas[6];
				String wheelchairAcc = lineas[7];
				String bicycleAcc = lineas[8];

				VOTrips vor = new VOTrips(routId,serviceID,tripID,headsign,sName,direccionID,blockID,wheelchairAcc,bicycleAcc);
				Trips.agregar(vor);
			}

			br.close();


		} 
		catch (IOException e1) 
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}				
	}

	@Override
	public void loadStopTimes(String stopTimesFile) 
	{
		String linea;
		BufferedReader br = null;
		String splitBy = ",";

		// TODO Auto-generated method stub
		try 
		{

			br = new BufferedReader(new FileReader(stopTimesFile));
			StopTimes = new DoubleLinkedList<VOStoptimes>();
			br.readLine();
			while( (linea = br.readLine()) != null)
			{
				String[] lineas = linea.split(splitBy);
				int tripID = Integer.parseInt((lineas[0]));
				String departureTime = lineas[1];
				String arrivalTime = lineas[2];
				int stopID = Integer.parseInt(lineas[3]);
				String sequence = lineas[4];
				String headsign = lineas[5];
				String pickUpType = (lineas[6]);
				String dropOffType = lineas[7];

				VOStoptimes vor = new VOStoptimes(tripID, departureTime, arrivalTime,stopID, sequence, headsign, pickUpType, dropOffType);
				StopTimes.agregarFinal(vor);
			}
			System.out.println(StopTimes.Size());
			br.close();

		} 
		catch (IOException e1) 
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}		
	}

	public ArrayList<VOStop> tempList = new ArrayList<>();
	
	@Override
	public void loadStops(String stopsFile) 
	{
		String linea;
		String splitBy = ",";

		// TODO Auto-generated method stub
		try 
		{

			BufferedReader br = new BufferedReader(new FileReader(stopsFile));
			Stops = new RingList<VOStop>();
			br.readLine();
			int x = 0;
			while( (linea = br.readLine()) != null)
			{
				
				String[] lineas = linea.split(splitBy);
				int stopID = Integer.parseInt((lineas[0]));
				String stopCode = lineas[1];
				String stopName = lineas[2];
				String stopDesc = lineas[3];
				String stopLat = lineas[4];
				String stopLon = lineas[5];
				String zoneID =lineas[6];
				String stopURL = lineas[7];
				String locationType = lineas[8];

				VOStop vor = new VOStop(stopID, stopCode, stopName, stopDesc, stopLat, stopLon, zoneID, stopURL, locationType);
				Stops.agregar(vor);
				tempList.add(vor);
			}

			//System.out.println(Stops.Size());
			br.close();

		} 
		catch (IOException e1) 
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}		
		sortingHat(tempList);
		
		for (int i = 0; i < tempList.size(); i++) {
			Stops.agregar(tempList.get(i));
		}
		
	}

	private void intercambio(int x, int y, RingList<VOStop> lista)
	{
		Node<VOStop> xx = lista.get(x);
		Node<VOStop> yy = lista.get(y);
		
		if(xx.darPrev() != null && yy.darPrev() != null)
		{
			Node<VOStop> temp = lista.get(x);
			xx.darNext().cambiarPrev(yy);
			xx.darPrev().cambiarNext(yy);
			
			xx.cambiarNext(yy.darNext());
			xx.cambiarPrev(yy.darPrev());
			
			yy.darNext().cambiarPrev(xx);
			yy.darPrev().cambiarNext(xx);
			
			yy.cambiarPrev(temp.darPrev());
			yy.cambiarNext(temp.darNext());
			
		}
		else if(xx.darPrev() == null && yy.darNext() != null)
		{
			Node<VOStop> temp = lista.get(x);
			xx.darNext().cambiarPrev(yy);
			
			xx.cambiarNext(yy.darNext());
			xx.cambiarPrev(yy.darPrev());
			
			yy.darNext().cambiarPrev(xx);
			yy.darPrev().cambiarNext(xx);
			
			yy.cambiarPrev(null);
			yy.cambiarNext(temp.darNext());
		}
		else if(xx.darPrev() != null && yy.darNext() == null)
		{
			Node<VOStop> temp = lista.get(x);
			xx.darNext().cambiarPrev(yy);
			xx.darPrev().cambiarNext(yy);
			
			xx.cambiarNext(null);
			xx.cambiarPrev(yy.darPrev());
			
			yy.darPrev().cambiarNext(xx);
			
			yy.cambiarPrev(temp.darPrev());
			yy.cambiarNext(temp.darNext());
		}
	}
	
	
	public void sortingHat(ArrayList<VOStop> lista)
	{
		for (int i = 1; i < lista.size(); i++) {
			boolean terminado = false;
			for (int j = 1; j > 0 && !terminado; j--) {
				VOStop t = lista.get(j);
				VOStop actual = lista.get(j - 1);
				if (t.zoneID().replaceAll("\\s+", "").compareTo(actual.zoneID().replaceAll("\\s+", "")) > 0 || 
						t.zoneID().replaceAll("\\s+", "").compareTo(actual.zoneID().replaceAll("\\s+", ""))==0) {
					lista.set(j, actual);
					lista.set(j - 1, t);
				} else {
					terminado = true;
				}
			}

		}
	}
	
	




	@Override
	public IList<VORoute> routeAtStop(String stopName) throws Exception
	{
		
				QuickSort<VOStop> sort = new QuickSort<VOStop>();
				sort.quickSort(tempList, 0, tempList.size()-1);
				
				sortingHat(tempList);
				
				for (int i = 0; i < tempList.size(); i++) 
				{
					Stops.agregar(tempList.get(i));
				}
		listaRecursivaStops(ListaStopsOrganizada, Stops.get(0));
		listaRecursivaTrips(ListaTripsOrganizada, Trips.get(0));

		
		ArrayList<Integer> stopidees = new ArrayList<Integer>();
		ArrayList<Integer> tripidees = new ArrayList<Integer>();
		ArrayList<Integer> routeidees = new ArrayList<Integer>();
		DoubleLinkedList<String> routeName = new DoubleLinkedList<String>();

		for (int i = 0; i < tempList.size(); i++) {
			if(tempList.get(i).getName().equals(stopName))
			{
				stopidees.add(tempList.get(i).id());
			}
		}
		for(int i = 0; i<StopTimes.Size(); i++)
		{
			for(int j = 0; j<stopidees.size(); j++)
			{
				
				if(stopidees.get(j) == StopTimes.get(i).giveInfo().stopId())
				{
					tripidees.add(StopTimes.get(i).giveInfo().TripId());
				}
			}
		}
		for (int i = 0; i < Trips.Size(); i++) 
		{
			for (int j = 0; j < tripidees.size(); j++) {
					if(Trips.get(i).giveInfo().tripID().equals(tripidees.get(j)))
					{
						routeidees.add(Trips.get(i).giveInfo().routeId());
					}
				}
				
			}
		
		for (int i = 0; i < Routes.Size(); i++) 
		{
			for (int j = 0; j < routeidees.size(); j++) 
			{
				if(Routes.get(i).giveInfo().id() == (routeidees.get(j)))
				{
					routeName.add(new Node<String>(Routes.get(i).giveInfo().getName()));
				}
			}
		}
		
		return routeName;

	}

	@Override
	public IList<VOStop> stopsRoute(String routeName, String direction) throws Exception {
		// TODO Auto-generated method stub
		int routeId = 0;
		DoubleLinkedList<Integer> listaTrips = new DoubleLinkedList<Integer>();

		DoubleLinkedList<Integer> listaDirecciones = new DoubleLinkedList<Integer>();

		DoubleLinkedList<Integer> listaStops = new DoubleLinkedList<Integer>();

		DoubleLinkedList<VOStop> respu = new DoubleLinkedList<VOStop>();
		
		boolean seLogra = false;
		
		for (int i = 0; i < Routes.Size()&& !seLogra; i++) 
		{
			
			Node<VORoute> actual = Routes.get(i);
			if(routeName.trim().equals(actual.giveInfo().getName().trim()))
			{
				routeId = actual.giveInfo().id();
				seLogra = true;
			}
			System.out.println("Recorre rutas");
		}
		for (int i = 0; i < Trips.Size(); i++) 
		{
			VOTrips trip = Trips.get(i).giveInfo();
			if(trip.routeId() == routeId)
			{
				listaTrips.agregarFinal(trip.routeId());
			}
			System.out.println("Recorre agregaListas");
			System.out.println(listaDirecciones.Size());
			System.out.println(listaTrips.Size());
		}
		for (Node<Integer> nodoTrips = listaTrips.darPrimero(); nodoTrips.darNext() != null; nodoTrips = nodoTrips.darNext()) 
		{
			for (Node<VOStoptimes> nodo = StopTimes.darPrimero(); nodo.darNext() != null ; nodo = nodo.darNext()) 
			{
				int tripId = nodo.giveInfo().TripId();
				int tripId2 = nodoTrips.giveInfo().intValue();
				if(tripId == nodoTrips.giveInfo().intValue() && Integer.parseInt(direction) == 0)
				{
					listaStops.agregarComienzo(nodo.giveInfo().stopId());
				}
				else if(tripId == nodoTrips.giveInfo().intValue() && Integer.parseInt(direction) == 1)
				{
					listaStops.agregarFinal(nodo.giveInfo().stopId());
				}
				System.out.println(listaStops.Size());
			}
			
			System.out.println("SISA 7");
		}

		for (int i = 0; i < Stops.Size(); i++) 
		{
			for (int j = 0; j < listaStops.Size(); j++) 
			{
				if(listaStops.get(j).giveInfo() == Stops.get(i).giveInfo().id())
				{
					respu.agregarFinal(Stops.get(i).giveInfo());
					System.out.println(Stops.Size());
				}
			}
		}



		return respu;
	}
}
