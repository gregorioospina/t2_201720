package model.vo;

public class VOTrips {
	
	private int routeID;
	private String serviceID;
	private String tripID;
	private String headsign;
	private String Sname;
	private int direccionID;
	private String blockID;
	private String wheelchairAcc;
	private String bicycleAcc;
	
	
	public VOTrips(int rID, String sID, String tID, String Hsign, String sname, int dID, String bID,String wAcc,String bAcc)
	{

		routeID = rID;
		serviceID = sID;
		tripID = tID;
		headsign = Hsign;
		Sname = sname;
		direccionID = dID;
		blockID =bID;
		wheelchairAcc = wAcc;
		bicycleAcc = bAcc;
	}

	public String getName()
	{
		return Sname;
	}
	
	public String tripID()
	{
		return tripID;
	}
	
	public int routeId()
	{
		return routeID;
	}
	
	public int direccionID()
	{
		return direccionID;
	}
	
	
	
	
}
