package model.vo;

import model.data_structures.IList;
import model.data_structures.RingList;

/**
 * Representation of a route object
 */
public class VORoute 
{
	
	private int routeId;
	private String agencyId;
	private String routeSName;
	private String routeLName;
	private String routeDesc;
	private String routeType;
	private String routeURL;
	private String routeColor;
	private String routeTextColor;
	
	private IList<VOTrips> listaTrips;
	
	public VORoute(int id, String ai , String sName, String lName, String desc, String type, String URL, String color, String textColor)
	{
		routeId = id;
		agencyId = ai;
		routeSName = sName;
		routeLName = lName;
		routeDesc = desc;
		routeType = type;
		routeURL = URL;
		routeColor = color;
		routeTextColor = textColor;
	}

	/**
	 * @return id - Route's id number
	 */
	public int id() 
	{
		// TODO Auto-generated method stub
		return routeId;
	}

	/**
	 * @return name - route name
	 */
	public String getName() 
	{
		// TODO Auto-generated method stub
		return routeSName;
	}
	

}
