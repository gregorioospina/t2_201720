package model.vo;

public class VOStoptimes {

	private int tripID;
	private String departureTime;
	private String arrivalTime;
	private int stopID;
	private String sequence;
	private String headsign;
	private String pickUpType;
	private String dropoffType;
	
	
	public VOStoptimes(int tID, String dTime, String aTime,int StopID, String sqnce, String hsign, String pType,String dpType )
	{
		
		tripID = tID;
		departureTime = dTime;
		arrivalTime = aTime;
		stopID = StopID;
		sequence = sqnce;
		headsign = hsign;
		pickUpType = pType;
		dropoffType = dpType;
		
	}
	/**
	 * @return id - stop's id
	 */
	public int TripId() {
		// TODO Auto-generated method stub
		return tripID;
	}
	public int stopId(){
		return stopID;
	}

	/**
	 * @return name - stop name
	 */
	public int getName() {
		// TODO Auto-generated method stub
		return tripID;
	}
	
	public int tripID()
	{
		return tripID;
	}
	

	
	
}
