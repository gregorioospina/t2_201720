package model.vo;

/**
 * Representation of a Stop object
 */
public class VOStop {

	private int stopId;
	private String stopCode;
	private String stopName;
	private String stopDesc;
	private String stopLat;
	private String stopLon;
	private String zoneId;
	private String stopURL;
	private String locationType;

	public VOStop(int id, String code, String name, String desc, String lat, String lon, String zoneID2, String URL, String locationT)
	{
	
		stopId = id;
		stopCode = code;
		stopName = name;
		stopDesc = desc;
		stopLat = lat;
		stopLon = lon;
		zoneId = zoneID2;
		stopURL = URL;
		locationType = locationT;
		
	}
	/**
	 * @return id - stop's id
	 */
	public int id() {
		// TODO Auto-generated method stub
		return stopId;
	}

	/**
	 * @return name - stop name
	 */
	public String getName() {
		// TODO Auto-generated method stub
		return stopName;
	}
	
	public String zoneID()
	{
		return zoneId;
	}

}
