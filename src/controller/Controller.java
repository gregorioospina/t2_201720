package controller;

import java.io.IOException;

import api.ISTSManager;
import model.data_structures.DoubleLinkedList;
import model.data_structures.IList;
import model.data_structures.RingList;
import model.logic.STSManager;
import model.vo.VORoute;
import model.vo.VOStop;

public class Controller {

	/**
	 * Reference to the routes and stops manager
	 */
	private static ISTSManager  manager = new STSManager();

	public static void loadRoutes() {

		String x = "data/routes.txt";
		manager.loadRoutes(x);

	}

	public static void loadTrips() {

		
		try {
			String x = "data/trips.txt";
			manager.loadTrips(x);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void loadStopTimes() 
	{
		String x = "data/stop_times.txt";
		manager.loadStopTimes(x);
	}

	public static void loadStops() {
		
		String x = "data/stops.txt";
		manager.loadStops(x);

	}

	public static IList<VORoute> routeAtStop(String stopName) 
	{
		IList<VORoute> respu = new DoubleLinkedList<VORoute>();
		try {
			respu =  manager.routeAtStop(stopName);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return respu;
	}

	public static IList<VOStop> stopsRoute(String routeName, String direction) 
	{
		IList<VOStop> respu = new RingList<VOStop>();
		try 
		{
			respu = manager.stopsRoute(routeName, direction);
		} 
		catch (Exception e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return respu;
	}
}
