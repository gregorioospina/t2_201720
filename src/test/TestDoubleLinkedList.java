package test;

import static org.junit.Assert.*;
import model.data_structures.DoubleLinkedList;
import model.data_structures.Node;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

public class TestDoubleLinkedList<T extends String> {

	@Test
	public void test() 
	{
		testSize();
		testAgregarComienzo1();
		testAgregarComienzo2();
		testAgregarFinal();
		testEliminar();
		testEliminar2();
		testGet();
		testSize();
	}
	
	
	
	

private DoubleLinkedList<Node<String>> lista;
	
	/*
	 * El primer caso es una lista doblemente conectada vacia.
	 */

	public void caso1()
	{
		lista = new DoubleLinkedList<Node<String>>();
	}
	
	/*
	 * El segundo caso es una lista doblemente enlazada con 5 nodos.
	 */
	
	public void caso2()
	{
		lista = new DoubleLinkedList<Node<String>>();
		lista.agregarComienzo(new Node("Nodo 3"));
		lista.agregarFinal(new Node("Nodo 4"));
		lista.agregarComienzo(new Node("Nodo 2"));
		lista.agregarComienzo(new Node("Nodo 1"));
		lista.agregarFinal(new Node("Nodo 5"));
	}
	
	/*
	 * Test del tama�o
	 */
	
	
	@Test
	public void testSize()
	{
		caso2();
		assert  5 == lista.getSize() : "El tama�o de la lista no es el esperado";
	}
	
	/*/
	 * Test de agregar al comienzo 1
	 */
	@Test
	public void testAgregarComienzo1()
	{
		caso1();
		lista.agregarComienzo(new Node<>("Primer nodo agregado"));
		try 
		{
			assert( "Primer nodo agregado".equals(lista.get(0).giveInfo()) ): "No se obtuvo el resultado esperado";
			lista.agregarComienzo(new Node("Segundo nodo agregado"));
			assert ( "Segundo nodo agregados" ).equals(lista.get(1).toString()) : "No se obtuvo el resultado esperado";
		} 
		catch (Exception e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	/*
	 * Test de agregar al comienzo 2 El popo es homosexual tambi�n la x es homosexual pero la 
	 */
	@Test
	public void testAgregarComienzo2()
	{
		caso2();
		lista.agregarComienzo(new Node( "Primer nodo agregado" ));
		try 
		{
			assertTrue ("Primer nodo agregado".equals(lista.get(0).toString()));
			lista.agregarComienzo(new Node( "Segundo nodo agregado" ));
			assertTrue (lista.get(0).giveInfo().giveInfo().equals("Segundo nodo agregado") )  ;
		} 
		catch (Exception e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	@Test
	public void testAgregarFinal()
	{
		caso2();
		lista.agregarFinal(new Node( "Segundo nodo agregado"));
		try 
		{
			assertTrue ("Segundo nodo agregado".equals(lista.get(5).giveInfo().giveInfo()));
			lista.agregarFinal(new Node( "Primer nodo agregado"));
			assertTrue ("Primer nodo agregado".equals(lista.get(6).giveInfo().giveInfo()));
		} 
		catch (Exception e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	@Test
	public void testEliminar()
	{
		caso1();
		lista.agregarComienzo(new Node( "Primer nodo agregado" ));
		lista.deleteAtK(0);
		assertTrue (lista.getSize() == 0);
	}
	@Test
	public void testEliminar2()
	{
		caso2();
		lista.deleteAtK(1);
		System.out.println(lista.getSize());
		assertTrue (lista.getSize() == 4);
	}
	@Test
	public void testGet()
	{
		caso2();
		try 
		{
			assertTrue (lista.get(1).toString().equals("Nodo 2")) ;
		}
		catch (Exception e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
