package test;

import static org.junit.Assert.*;
import model.data_structures.DoubleLinkedList;
import model.data_structures.Node;
import model.data_structures.RingList;

import org.junit.Test;

public class TestRingList 
{

	
	@Test
	public void test() 
	{
		testSize();
		testAgregarComienzo1();
		testAgregarComienzo2();
		testAgregarFinal();
		testEliminar();
		testEliminar2();
		testGet();
		testSize();
	}
	
	RingList lista;
	
	
	public void caso1()
	{
		lista = new RingList();
	}
	
	/*
	 * El segundo caso es una lista doblemente enlazada con 5 nodos.
	 */
	
	public void caso2()
	{
		lista = new RingList();
		lista.agregar(new Node("Nodo 3"));
		lista.agregar(new Node("Nodo 4"));
		lista.agregar(new Node("Nodo 2"));
		lista.agregar(new Node("Nodo 1"));
		lista.agregar(new Node("Nodo 5"));
	}
	
	/*
	 * Test del tama�o
	 */
	
	public void testSize()
	{
		caso2();
		assert  5 == lista.Size() : "El tama�o de la lista no es el esperado";
	}
	
	/*/
	 * Test de agregar al comienzo 1
	 */
	
	public void testAgregarComienzo1()
	{
		caso1();
		lista.agregar(new Node<>("Primer nodo agregado"));
		try 
		{
			assert( "Primer nodo agregado".equals(lista.get(0).giveInfo()) ): "No se obtuvo el resultado esperado";
			lista.agregar(new Node("Segundo nodo agregado"));
			assert ( "Segundo nodo agregado" ).equals(lista.get(1).toString()) : "No se obtuvo el resultado esperado";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	/*
	 * Test de agregar al comienzo 2 El popo es homosexual tambi�n la x es homosexual pero la 
	 */
	
	public void testAgregarComienzo2()
	{
		caso2();
		lista.agregar(new Node( "Primer nodo agregado" ));
		try 
		{
			assert ("Primer nodo agregado".equals(lista.get(0).toString())) : "No se obtuvo el resultado esperado";
			lista.agregar(new Node( "Segundo nodo agregado" ));
			assert ("Segundo nodo agregado".equals(lista.get(0).toString())) : "No se obtuvo el resultado esperado";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void testAgregarFinal()
	{
		caso2();
		lista.agregar(new Node( "Segundo nodo agregado"));
		try 
		{
			assert ("Segundo nodo agregado".equals(lista.get(5))) : "No se obtuvo el resultado esperado";
			lista.agregar(new Node( "Primer nodo agregado"));
			assert ("Segundo nodo agregado".equals(lista.get(6))) : "No se obtuvo el resultado esperado";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	public void testEliminar()
	{
		caso1();
		lista.agregar(new Node( "Primer nodo agregado" ));
		lista.eliminarNodo(0);
		assert lista.Size() == 0 : "No se obtuvo el resultado esperado";
	}
	
	public void testEliminar2()
	{
		caso2();
		lista.eliminarNodo(0);
		assert lista.Size() == 5 : "No se obtuvo el resultado esperado";
	}
	
	public void testGet()
	{
		caso2();
		try 
		{
			assert lista.get(1).toString().equals("Nodo 2") : "No se obtuvo el resultado esperado";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
